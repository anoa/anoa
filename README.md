# Anoa

Anoa lets you instantly create channels where you can ask and answer questions totally anonymously.
You can try a demo here: [https://mysterious-meadow-85770.herokuapp.com/](https://mysterious-meadow-85770.herokuapp.com/).
Create a channel and share the ID with someone so they can join too.